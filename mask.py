# -*- coding: utf-8 -*-
#from tensorflow.keras.models import Sequential
import cv2
import numpy as np
import os
from tensorflow import keras
from werkzeug.utils import secure_filename
from flask import Flask, request, render_template, send_from_directory
import uuid

app = Flask(__name__)
APP_ROOT = os.path.dirname(os.path.abspath(__file__))
model = keras.models.load_model('mask_detection_model.h5')

@app.route("/")
def index():
    return render_template("h.html")

labels_dict=['With mask','WithoutMask','Improper']

def mask_detect(img):
  faceCascade = cv2.CascadeClassifier(cv2.data.haarcascades + "haarcascade_frontalface_default.xml")  

  faces = faceCascade.detectMultiScale(
          img,
          scaleFactor=1.3,
          minNeighbors=5,
          minSize=(30, 30)
  )
  print(faces)
  for (x, y, w, h) in faces:
      
    roi = img[y:y + h, x:x + w]
    roi=cv2.resize(roi,(100,100))
    roi = cv2.cvtColor(roi,cv2.COLOR_BGR2GRAY)
    #roi = np.array(roi)/255
    roi = np.reshape(roi,(1,100,100,1))
    roi.shape
    result=model.predict(roi)
    label=np.argmax(result,axis=1)[0]
    label = int(label)
    #cv2_imshow(img1)
    print(label)
    return labels_dict[label]

@app.route('/upload/<filename>')
def send_image(filename):
    return send_from_directory("upload", filename)


@app.route("/upload", methods=["POST"])
def upload():
   if request.method == 'POST':
      target = os.path.join(APP_ROOT, 'upload/')
      print(target)
      if not os.path.isdir(target):
            os.mkdir(target)
      else:
          print("Couldn't create upload directory: {}".format(target))
      print(request.files.getlist("file"))
      for upload in request.files.getlist("file"):
        print(upload)
        print("{} is the file name".format(upload.filename))
        filename = 'result_'+str(uuid.uuid4())+'.jpg'
        destination = "/".join([target, filename])
        print ("Save it to:", destination)
        upload.save(destination)
        img = cv2.imread(destination)
        result = mask_detect(img)
        print(result)
   return render_template("display.html", image_name=filename, result = result)
#app.route("/capture", methods=["POST","GET"])
#ef capture():
    
    
    
@app.route("/live")
def live():
     return render_template("live.html")

if __name__ == "__main__":
    app.run()

